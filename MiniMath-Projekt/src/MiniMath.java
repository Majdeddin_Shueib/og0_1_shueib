
public class MiniMath {
	
	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static int berechneFakultaet(int n){
		if (n == 0 || n == 1) {
			return 1;
		} else {
			int temp = 1;
			int schl = n;
			for (int i = 0; i < schl; i++) {
				temp = temp * n;
				n--;
			}
			return temp;
		}
		
	}
	
	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * @param n - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n 
	 */
	public static int berechneZweiHoch(int n){
		if (n < 31) {
			int temp = 2;
			for (int i = 0; i < n - 1; i++) {
				temp = temp * 2; 
			}
			return temp;
		} else {
		return 0;
	}
	}
	
	/**
	 * Die Methode berechnet die Summe der Zahlen von 
	 * 1 bis n (also 1+2+...+(n-1)+n)
	 * @param n - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n){
		int temp = 0;
		int schl = n;
		for (int i = 0; i < schl; i++) {
			temp += n;
			n--;
		}
		return temp;
	}
	
}

