package oszimt;

public class Quicksort {
	private long vertauschungen = 0;
	int counter = 0;

	  public int teilung (final int liste[], final int first, final int last ) {
		  int pivot = liste [(first + last) / 2];
		  int positionLinks = first;
		  int positionRechts = last;
		  
		  while (positionLinks <= positionRechts) {
			  while (liste[positionLinks] < pivot) {
				  positionLinks++;
			  }
			  while (liste[positionRechts] > pivot) {
				  positionRechts--;
			  }
			  if (positionLinks <= positionRechts) {
				  counter++;
				  int tmp = liste [positionLinks];
				  liste[positionLinks] = liste[positionRechts];
				  liste[positionRechts] = tmp;
				  positionLinks++;
				  positionRechts--;
				  inkrementVertauschungen();
			  }
			  
			  
			  for (int i = 0; i < liste.length; i++) {
				  System.out.print(liste[i] + ", ");
			  }
			  System.out.println("");
		  }
		  return positionLinks;
	  }


		private void inkrementVertauschungen() {
		  vertauschungen++;
		}
		public long getVertauschungen() {
			return this.vertauschungen;
		}


		public void sort (int liste[], int first, int last) {
	    	int index = teilung(liste, first, last);
	    	if (first < index - 1) {
	    		sort(liste, first, index - 1);
	    		counter++;
	    	}
	    	if (index < last) {
	    		sort(liste, index, last);
	    		counter++;
	    	}
	    	
		  
	  }

	}