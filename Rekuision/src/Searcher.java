
public class Searcher {
	
	public static int searcher3(int[] zahlen, int min, int max, long zuSuchen) {
        if (max >= min && min <= zahlen.length - 1) {
            int mid = min + ((max - min) / 2);
        if (zahlen[mid] == zuSuchen) 
        	return mid;

        if (zahlen[mid] > zuSuchen) 
        	return searcher3(zahlen, min, mid - 1, zuSuchen);

            return searcher3(zahlen, mid + 1, max, zuSuchen);


        }
        return -1;

    }

}
