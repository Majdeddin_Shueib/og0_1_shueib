package oszimt;

import java.util.Random;

public class Lineare_Suche {

	private final int nicht_gefunden = -1;

	public static int sucheWert(long[] zahlen, long gesuchtezahl) {

		final int nicht_gefunden = -1;
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i] == gesuchtezahl) {
				return i;
			}

		}
		return nicht_gefunden;
	}
	public static long[] getSortedList(int laenge) {
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;

		for (int i = 0; i < laenge; i++) {
			naechsteZahl += rand.nextInt(3) + 1;
			zahlenliste[i] = naechsteZahl;
		}

		return zahlenliste;
	}
}
