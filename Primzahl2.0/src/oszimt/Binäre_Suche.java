package oszimt;

public class Bin�re_Suche {
	
	private final int nicht_gefunden = -1;
	private int arrayLaenge;
	private int z = 0;
	private int i;
	
	public int sucheWert(int[] zahlen, int gesuchteZahl) {
		arrayLaenge--;
		while (z <= arrayLaenge) {
			i = (z + arrayLaenge) / 2;
			if (zahlen[i] == gesuchteZahl) {
				return i;
			}	else if (zahlen[i] > gesuchteZahl) {
				arrayLaenge = i - 1;
			}   else {
				arrayLaenge = i + 1;
			}
		}
		return nicht_gefunden;
	}

}
