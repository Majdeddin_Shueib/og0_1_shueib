package oszimt;
import java.util.Scanner;
public class PrimzahlTest {

	public static void main(String[] args) {		
	Scanner sc = new Scanner(System.in);	
	long zahl;
	boolean erg;
	long time;
	long timeF;
	System.out.println("Wie viele Zahlen?: ");
    long[] zl = new long [sc.nextInt()];
    System.out.println("Zahlen eingeben: ");
    for (int i = 0; i < zl.length; i++) {
    	zl[i] = sc.nextLong();
    }
    System.out.println("Zahl   \t   isPrimzahl \t   Zeit \t");
    for (int i = 0; i < zl.length; i++) {
    	
    
	time = System.currentTimeMillis();
	erg = Primzahl.isPrimzahl(zl[i]);
	timeF = System.currentTimeMillis() - time;
	System.out.println(zl[i] +   "\t"   +    erg   +   "\t"   +   timeF  +   "ms \t");
	}
	}
}
