
public class KeyStore {

	private String[] key;
	public int currentPOS;
	public final int MAXPOS = 200;

	public KeyStore() {
		this.key = new String[MAXPOS];
		this.currentPOS = 0;
	}

	public KeyStore(int length) {
		this.key = new String[length];
		this.currentPOS = 0;
	}

	public boolean add(String eintrag) {
		if (currentPOS < MAXPOS) {
			this.key[this.currentPOS] = eintrag;
			this.currentPOS++;
			return true;
		} else {
			return false;
		}
	}

	public int indexOf(String eintrag) {
		final int nichtGefunden = -1;
		for (int i = 0; i < this.currentPOS; i++) {
			if (this.key[i].equals(eintrag)) {
				return i;
			}
		}
		return nichtGefunden;
	}

	public void remove(int index) {
		if (index >= 0 && index < currentPOS) {
			key[index] = "";
			for (int i = index; i < this.currentPOS; i++) {
				key[i] = key[i + 1];

			}
			currentPOS--;
		}
	}

	public String[] key() {
		return key;
	}

}
