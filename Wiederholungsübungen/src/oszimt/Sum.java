package oszimt;

import java.util.Scanner;

public class Sum {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Geben Sie einen Grenzwert ein: ");
		int gw = scan.nextInt();
		int z = 2;
		int erg = 0;
		scan.close();
		
		while (z <= gw) {
        erg = erg + z;
        z = z + 2;
		}
        System.out.print("Ihr Ergebnis lautet: " + erg);	
	
	}
}
