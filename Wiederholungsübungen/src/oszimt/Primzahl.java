package oszimt;

import java.util.Scanner;

public class Primzahl {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		long pz = scan.nextLong();
		boolean primzahl = true;
		scan.close();

		for (int i = 2; i < pz; i++) {
			if (pz % i == 0) {
				primzahl = false;
			}

		}
		if (primzahl == true) {
			System.out.println("Es ist eine Primzahl: " + pz);
		} else {
			System.out.println("Es ist keine Primzahl: " + pz);
		}
	}

}
