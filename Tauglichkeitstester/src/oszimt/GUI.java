package oszimt;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JRadioButton;
import java.awt.CardLayout;
import java.awt.Dimension;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;

public class GUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Tauglichkeitstester");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 721, 609);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		
		JButton btnStart = new JButton("Start");
		btnStart.setPreferredSize(new Dimension(100, 50));
		panel_1.add(btnStart);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.EAST);
		
		JButton btnStopp = new JButton("Stopp");
		btnStopp.setPreferredSize(new Dimension(100, 50));
		panel_2.add(btnStopp);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3, BorderLayout.WEST);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JRadioButton rdbtnReaktion = new JRadioButton("Reaktion");
		panel_3.add(rdbtnReaktion, BorderLayout.NORTH);
		
		JRadioButton rdbtnKonzentration = new JRadioButton("Konzentration");
		panel_3.add(rdbtnKonzentration, BorderLayout.SOUTH);
		
		JRadioButton rdbtnEinschaetzung = new JRadioButton("Einsch\u00E4tzung");
		panel_3.add(rdbtnEinschaetzung, BorderLayout.WEST);
		
		JPanel Cardlayout = new JPanel();
		contentPane.add(Cardlayout, BorderLayout.CENTER);
		Cardlayout.setLayout(new CardLayout(0, 0));
		
		JPanel panel_4 = new JPanel();
		Cardlayout.add(panel_4, "name_5211819965700");
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JTextArea txtrNachDemBettigen = new JTextArea();
		txtrNachDemBettigen.setLineWrap(true);
		txtrNachDemBettigen.setText("Reaktion\r\n\r\nNach dem Bet\u00E4tigen der Start-Schaltfl\u00E4che, erscheint eine Ampel, die rot zeigt. Beim Umspringen auf gr\u00FCn, muss die Stopp-Schaltfl\u00E4che gedr\u00FCckt werden. Es wird anschlie\u00DFend die Reaktionszeit in s angezeigt.\r\n\r\n");
		txtrNachDemBettigen.setPreferredSize(new Dimension(250, 22));
		txtrNachDemBettigen.setWrapStyleWord(true);
		panel_4.add(txtrNachDemBettigen, BorderLayout.EAST);
		
		JPanel panel_7 = new JPanel();
		panel_4.add(panel_7, BorderLayout.SOUTH);
		
		JLabel lblNewLabel = new JLabel("Auswertung:");
		panel_7.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setIcon(new ImageIcon(GUI.class.getResource("/bilder/circle-23965_960_720.png")));
		panel_4.add(lblNewLabel_1, BorderLayout.CENTER);
		
		JPanel panel_5 = new JPanel();
		Cardlayout.add(panel_5, "name_5213908869900");
		
		JPanel panel_6 = new JPanel();
		Cardlayout.add(panel_6, "name_5222739163800");
	}

}
