package oszimt;

public abstract class Test {
	
	protected boolean aktiv;
	protected double ergebnis;
	
	
	protected abstract void warten();
		
	
	
	public abstract double getErgebnis(); 
	
	
	
	public abstract boolean isAktiv(); 
		
	
	
	public abstract void starten();
		
	
	
	public abstract void stoppen(); 
		
	
	
	public abstract void zeigeHilfe();
		
	

}
